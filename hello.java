import java.util.Scanner;
//import HttpMethods;


class Hello implements OurCommunicationInterface { //160920 | Interface implementation


  enum ThreadTypes {
    DateReporting,    //excercise 2
    HTTPGetter        //exercise 3
  }

  //ArrayList<DesktopActivity> activities = new ArrayList<DesktopActivity>();
  //Thread1 ourOwnThread = null; //160920 | moving this || hmm, actually threads didn't like this
  //  ..it stopped them from restarting.
  Scanner scanner = null; //making it global


  public static void main(String[] args)
  {
    /* 160920 | Moving everything into it's own method - darn I really need that version control
    System.out.println("Hello world! The app is on.");

    //Thread1 ourOwnThread = new Thread1();
    //ourOwnThread.start();

    Scanner scanner = new Scanner(System.in);
    String command = scanner.nextLine();

    Thread1 ourOwnThread = null;

    if (command.equals("START") && ourOwnThread == null)
    {
      ourOwnThread = new Thread1();
      ourOwnThread.start();
    }

    if (command.equals("STOP"))
    {
      ourOwnThread.stop(); //interrupt(); //apparently both deprecated
      //ourOwnThrea.terminate(); //nvm, this is for IndexProcessor
      //http://stackoverflow.com/questions/10961714/how-to-properly-stop-the-thread-in-java

      //not allowed to restart threads, instead must create new instance.
      //ourOwnThread.start();  //results in error
    }

    scanner.nextLine();
    */
    //while(true);
    Hello ourMainActivity = new Hello();
    //ourStart();
    ourMainActivity.ourStart();
  } //End.main()

  public void ourStart()
  {
    System.out.println("Hello world! The app is on.");

    //launchThreads(listenToStartCommands());
    launchThreads(listenToStartCommandTypes());
    //if (listenToStartCommands())
      //launchThreads(); //entering recursive loop
    //Thread1 ourOwnThread = new Thread1();
    //ourOwnThread.start();

    /*
    Scanner scanner = new Scanner(System.in);
    String command = scanner.nextLine();

    Thread1 ourOwnThread = null;
    //ourOwnThread = null; //keeping it empty

    if (command.equals("START") && ourOwnThread == null)
    {
      ourOwnThread = new Thread1();
      ourOwnThread.setFollower(this);
      ourOwnThread.start();
    }

    //~~to avoid immediately exiting the application~~
    command = scanner.nextLine(); //silly me forgot to assign this to the string we query below
    if (command.equals("STOP")) //haha, not surprisingly, it didn't listen to this.
    {
      //works after getting here
      ourOwnThread.stop(); //interrupt(); //apparently both deprecated

      //ourOwnThread.
      //ourOwnThrea.terminate(); //nvm, this is for IndexProcessor
      //http://stackoverflow.com/questions/10961714/how-to-properly-stop-the-thread-in-java

      //not allowed to restart threads, instead must create new instance.
      //ourOwnThread.start();  //results in error

      System.out.println("Attempted to stop the thread. Press anykey to stop."); //works just fine after getting here
    }//end.if("STOP")


    //to avoid immediately exiting the application
    scanner.nextLine();
    */
  } //End.ourStart()

  public void launchThreads() // :(
  {
    launchThreads(ThreadTypes.DateReporting);
  } //End.launchThreads() - overload

  public void launchThreads(boolean toggle) {
    if (toggle)
      launchThreads(ThreadTypes.DateReporting);
    else
      launchThreads(listenToStartCommands());
  } //End.launchThreads() - overload
//of course JAVA doesn't support default parameters. Why am I not surprised. Overloading above.
  public void launchThreads(ThreadTypes types) //(Thread1 runningThread)
  {
      //will be used to continually listen to the
      // nextline and on every START start a new thread.
      //if (runningThread == null) //skip if null
        //return;

      //starting to feel like this needs several functions to enclose these. :|
      switch (types) {
          case HTTPGetter: { //ThreadTypes.HTTPGetter: { //this is really strict... :|
            //excercise 3 - getting text through http in another thread
            System.out.println("Please input the address: \n");
            scanner = new Scanner(System.in);
            String addressString = scanner.nextLine(); //messing with this in the fetch | vars didn't work
            HttpGetThread getter = new HttpGetThread(addressString, this);
            getter.start();

            /* Another possible way to call this
            String str;
            new Thread( new Runnable() {
              @Override
              public void run() {
                try {
                  str = fetchURL("https://bitbucket.org/Zaelot/javasystemprogrcourse/overview")
                }
                catch(Exception e){}
              }//end.run
            }//end.Runnable
            ).start(); //end.new Thread
            */

            break;
          }
         case DateReporting: { //ThreadTypes.DateReporting: {
          //excercise 1 & 2 below
          System.out.println("Starting a thread");
          Thread1 runningThread = null;
          runningThread = new Thread1();
          runningThread.setFollower(this);
          runningThread.start();

          //if (listenToCommands == (byte)2) {
          if (listenToStopCommands()) {
            runningThread.stop();
            System.out.println("Stopped a thread");
            //runningThread = null; //unnecessary
          }

          if (listenToStartCommands())
            launchThreads();
          break;
        }
        default:
          System.out.println("How did you even get here?");
          break;
      }//end.switch

      System.out.println("While waiting for the result: \n");
      launchThreads(listenToStartCommandTypes()); //inception;
  } //End.launchThreads()

  boolean listenToStopCommands() //recursively
  {
    if (scanner == null)
      scanner = new Scanner(System.in);

    System.out.println("Write STOP to stop the thread.");

    String commandInput = scanner.nextLine();
    if (commandInput.equals("STOP"))
      return true;
    else
      return listenToStopCommands();
    // else if (commandInput.equals("START"))
    //   return 1;
    // else
    //   return 0;
  } //End.listenToStopCommands()

  boolean listenToStartCommands() //recursively || excercise 2
  {
    if (scanner == null)
      scanner = new Scanner(System.in);

    System.out.println("Write START to begin a thread.");

    String commandInput = scanner.nextLine();
    if (commandInput.equals("START"))
      return true;
    else if (commandInput.equals("QUIT"))
      return false;
    else
      return listenToStartCommands();
  } //End.listenToStartCommands()

  ThreadTypes listenToStartCommandTypes() //excercise 3 onwards
  {
    if (scanner == null)
      scanner = new Scanner(System.in);

      System.out.println("Write START to begin DateReport, or FETCH to Fetch url that.");

      String commandInput = scanner.nextLine(); //geez, does var not work in Java?
      commandInput = commandInput.toUpperCase(); //because the case calling isn't an option, just converting
      //forgot to assign the darned thing...
      //System.out.println("Input: " + commandInput);
      switch (commandInput) {
        case "START": { //apparently it can't handle CASE :|
          return ThreadTypes.DateReporting;
          //break; //does it really need break in addition to the return?? Darnit this ATOM thing
          //in fact, it gave me a darned error for this. o_o
        }
        case "QUIT": {
          System.out.println("Quitting.");
          System.exit(0); //0 ok, 1 problems || equivalent to Runtime.getRuntime().exit(n) (also .halt())
          break; //ridiculous, likely completely unnecessary || and this isn't unreachable?!??!?s
        }
        case "FETCH": {
          return ThreadTypes.HTTPGetter;
          //break;
        }
          /*
        CASE "start": //ain't this awesome, can't call other cases. :|
          return ThreadTypes.DateReporting;
          break;
        CASE "quit":
          System.out.println("Quitting.");
          System.exit(1);
          break;
          */
          default: { //unacceptable, go back and try again.
            return listenToStartCommandTypes();
          }
      }//end.switch

      //seriously compiler?!?!? The darned swtich took care of these.
      return ThreadTypes.DateReporting;
  } //End.listenToStartCommandTypes()

//interface example
/*
  public void createNewActivity(){
    DesktopActivity created = new DesktopActivity();
    created.onCreate();
    activities.add(created);
  }

  public void stopActivity(DesktopActivity activity)
  {
    activity.onPause();
    activity.onDestroy();
    activity.remove(activity);
  }
  */

  public void ourStringReporting(String ourString) //if not defined, automatically protected
  {
    System.out.println("HELLO: " + ourString);
  } //End.ourStringReporting() - Interface

} //End.Hello{}
