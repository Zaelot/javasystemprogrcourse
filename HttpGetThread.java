// didn't find these? | geez, there's a bit too many of these for a simple darn example
//import org.apache.commons.httpclient.*; //what this is all about
//import org.apache.commons.httpclient.methods.*; //statusline etc I guess?
//import org.apache.commons.httpclient.params.HttpMethodParams; //buffer? maybe
//oh well, let's do this the hard way, instead of using the java.net*
//~2016-10-04 | apparently it's impossible for me to include these on this machine. :|

//import HttpMethods.*;
//import org.springframework.http.HttpMethod;
//import java.net.*;
//import java.net.URL; // && .URI;
import java.io.*; //for the exceptions, and Readers
import java.net.*; //particularly for the URL and URLConnection
import java.nio.charset.Charset;
//seriously? the all engrossing astrisks are not enough??
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.lang.StringBuilder;
//import java.lang.*;
//also such things as
//import java.net.HttpURLConnection;
//import java.io.IOException;
//import java.io.InputStream;

//not really sure how many of these are needed...

public class HttpGetThread extends Thread {

  public static void socketSend(final String ip,final int port,final String msg){
      new Thread(new Runnable() {
          @Override
          public void run() {
              try {
                  Socket sock = new Socket(ip, port);
                  DataOutputStream out = new DataOutputStream(sock.getOutputStream());
                  out.writeBytes(msg);
                  sock.close();
              }catch(Exception e){}
          }
      }).start();
  } //End.socketSend()


  public static String fetchURL(String url) {
    //playing silly
    if ( url == null || url.isEmpty() )
      url = "https://bitbucket.org/Zaelot/javasystemprogrcourse/raw/93e2dab1d602fa257339032b2faea7c4260182e6/README.md";

    StringBuilder ourBuilder = new StringBuilder();
    URLConnection conn = null; //we'll need to instantiate a new one
    InputStreamReader inpStream = null;

    try {
      URL u=new URL(url);
      conn=u.openConnection();
      if (conn != null) {
        conn.setConnectTimeout(15000); //15sec || not really necessary
        conn.setReadTimeout(10000); //10sec
        //very annoying. Ran into trouble, because I wrote TimeOut -with capital O :|
        conn.setAllowUserInteraction(false); //likewise unnecessary, as well as the next one
        conn.setDoOutput(true);
      }
      if (conn != null && conn.getInputStream() != null) {
        inpStream = new InputStreamReader( conn.getInputStream(), Charset.defaultCharset() );
        //could also use
        //InputSource input = new InputSource(inpStream);
        BufferedReader buffRead = new BufferedReader(inpStream);
        if (buffRead != null) {
          int i; //0
          while ( (i = buffRead.read()) != -1 ) ourBuilder.append( (char)i );
          //cleverly both assigning the next value and checking whether it's finished
          buffRead.close();
        }//end.if(buffRead)
      }//end.if(getInputStream())
      inpStream.close();
    }//end.try
    catch (Exception e) {
      System.out.println("Encounterred error: " + e.getMessage()); //hope it's this kind of excecption?
      //Exceptions.handle(e);
    }
    return ourBuilder.toString(); //return received result, catch or not


    /* kinda doubt it, but saw this kind of example as well
    HttpResponse response = HttpRequest
    .create(new URI("http://www.stackoverflow.com"))
    .headers("Foo", "foovalue", "Bar", "barvalue")
    .GET()
    .response();
    */

  } //End.fetchURL()



    private String url;
    //private final int HTTP_OK = 200;
    private OurCommunicationInterface follower = null;//IPostResponse ipostObj;

    public HttpGetThread(String url, OurCommunicationInterface followerObj) {//IPostResponse ipostObj) {
        this.url = url;
        this.follower = followerObj;//ipostObj;
    } //End.HttpGetThread() - constructor

    public void run() {
        try {
          //String response = fetchURL(url);
          follower.ourStringReporting( fetchURL(url) );
        }
        catch (Exception e)
        {}

          /*
            HttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(url);
            HttpResponse httpResponse = httpClient.execute(httpGet);
            int responseCode = httpResponse.getStatusLine().getStatusCode();
            if (responseCode == HTTP_OK) {
                InputStream inputStream = httpResponse.getEntity().getContent();
                int bufferCount = 0;
                StringBuffer buffer = new StringBuffer();
                while ((bufferCount = inputStream.read()) != -1) {
                    buffer.append((char) bufferCount);
                }
                follower.getResponse(buffer.toString());
            }//end.if(OK)
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        */

    } //End.run()

} //End.HttpGetThread{}
