public interface DesktopActivityInterface {//extends ApplicationContext {
  //wonder if these are allowed to be protected? Likely not..
  protected void onCreate(Bundle savedInstanceState);
  protected void onStart();
  protected void onRestart();
  protected void onResume();
  protected void onPause();
  protected void onStop();
  protected void onDestroy();

  //void onLowMemory(); //extends Application
  //void onTerminate();

} //End.DesktopActivityInterface{}
