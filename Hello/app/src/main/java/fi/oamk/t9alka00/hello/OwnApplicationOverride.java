package fi.oamk.t9alka00.hello;

import android.app.Application;
import android.content.res.Configuration;
import android.util.Log;

/**
 * Created by t9alka00 on 13.9.2016.
 * Application override exercise based on https://www.mobomo.com/2011/05/how-to-use-application-object-of-android/
 */
public class OwnApplicationOverride extends Application {

    //Let's make this into a singleton as well
    private static OwnApplicationOverride singleton;

    public static OwnApplicationOverride getInstance() {
        return singleton;
    }

    //Application level callbacks
    @Override
    public void onCreate() {
        Log.d("Hello", "ApplicationOverride, onCreate method.");
        super.onCreate();
        singleton = this;
    } //End.onCreate() - Override

    @Override
    public void onLowMemory() {
        Log.d("Hello", "ApplicationOverride, onLowMemory method.");
        super.onLowMemory();
    } //End.onLowMemory() - Override

    @Override
    public void onTerminate() {
        Log.d("Hello", "ApplicationOverride, onTerminate method.");
        super.onTerminate();
    } //End.onTerminate() - Override

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        Log.d("Hello", "ApplicationOverride, onConfigurationChanged method.");
        super.onConfigurationChanged(newConfig);
    } //End.onConfigurationChanged() - Override

} //End.OwnApplicationOverride{}

//next task, add thread control along the lines of hello.java
