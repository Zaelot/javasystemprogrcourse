package fi.oamk.t9alka00.hello;

/**
 * Created by t9alka00 on 27.9.2016.
 */

public interface OurNetworkInterface {
        void operationReady(String data);
        void caughtError(int httpErrorCode);
}
