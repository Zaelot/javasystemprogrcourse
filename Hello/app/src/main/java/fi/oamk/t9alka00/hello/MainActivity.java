package fi.oamk.t9alka00.hello;

import android.support.annotation.UiThread;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.flaviofaria.kenburnsview.KenBurnsView; //~Z 2016-10-18 | for some odd reason tried to implement this as well. silly me.
import com.flaviofaria.kenburnsview.Transition;
//import android.view.View;

//import org.androidannotations.annotations.Background; //~Z 2016-10-18 | best to deprecate these alltogether, I remember reading this caused problems with JACK
//import org.androidannotations.annotations.EActivity; //~Z 2016-10-18 | argh, from what I understand these are needed after all, and should work. Troubling.
/*  As for Jack and Jill working with AnnotationProcessing for Android, there are already some open repos up that indicate it should work fine, just change `apt` to `annotationProcessor` in the build.gradle file.
* http://blog.stablekernel.com/the-10-step-guide-to-annotation-processing-in-android-studio */

//@EActivity //android annotations
public class MainActivity extends AppCompatActivity implements OurNetworkInterface{ //implements View.OnClickListener {

   // @ViewById(R.id.textView) //.myTextView) // Injects R.id.myTextView | if we had that?
    //@BindView(R.id.myTextView) //~Z 2016-10-18 | why is it highlighting it? What am I missing?? is it @Inject I should be using?
    //@BindView(R.id.textView) //well, seeing as apparently we're not using the myTextView anywhere
    //@Inject(R.id.textView) //nope, it's just complaining about that AppCompatActivity
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d("Hello", "Mainactivity, onCreate method");

        //Button namiska = (Button) findViewById(R.id.button);
        //namiska.setOnClickListener(this);
        //Toast.makeText(this, "Operation is ready", 5);


        findViewById(R.id.button).setOnClickListener( (v) ->  {
            //@Override
            //public void onClick(View v) {
                while (true) {
                        //; //was wondering about this, apparently it is causing problems? Just that the compilation errors are shown so weirdly.
                }
            });

        //ImageView imageView = (ImageView) findViewById(R.id.my_image_view);
        //KenBurnsView imageView = (KenBurnsView) findViewById(R.id.my_image_view);
        mImageView = (KenBurnsView) findViewById(R.id.imageView);

        Glide.with(this)
                .load("https://static.pexels.com/photos/3247/nature-forest-industry-rails.jpg") //("http://goo.gl/gEgYUd")
                .into(mImageView); //(imageView);
        mImageView.setTransition(this);

    } //End.onCreate() - override

    //Lab 6: Annotations

    @Override
    public void onTransitionStart(Transition transition)
    {

    } //End.onTransitionStart() - override

    @Override
    public void onTransitionEnd(Transition transition)
    {
        Glide.with(this)
                .load("http://weknowyourdreams.com/images/nature/nature-05.jpg")
                .into(mImageView);
    } //End.onTransitionEnd()

    @Background
    protected void loadSomethingOnTheBackground()
    {
        //TextView text = (TextView) findViewById(R.id.textView);
        //text.setText("Updated"); //?
        updateText("Our text from network");
    } //End.loadSomethingOnTheBackground

    @UiThread
    protected void updateText(String text)
    {

    } //End.updateText()

    /*
    @UiThread // Executed in the ui thread
    void showResult(String translatedText) {
        result.setText(translatedText);
        result.startAnimation(fadeIn);
    }

    @Click // When R.id.doTranslate button is clicked
    void doTranslate() {
         translateInBackground(textInput.getText().toString());
    }
    */

    @Override
    protected void onStart(){
        Log.d("Hello", "MainActivity, onStart method");
        super.onStart();
    } //End.onStart() - override

    @Override
    protected void onRestart(){
        Log.d("Hello", "MainActivity, onRestart method");
        super.onRestart();
    } //End.onRestart() - override

    @Override
    protected void onPause()
    {
        Log.d("Hello", "MainActivity, onPause method");
        super.onPause();
        //Stop GPS Monitoring
    } //End.onPause() - override

    @Override
    protected void onResume(){
        Log.d("Hello", "MainActivity, onResume method");
        super.onResume();
    } //End.onResume() - override

    @Override
    protected void onStop(){
        Log.d("Hello", "MainActivity, onStop method");
        super.onStop();
    } //End.onStop() - override

    @Override
    protected void onDestroy() {
        Log.d("Hello", "MainActivity, onPause method");
        super.onDestroy();
    } //End.onDestroy() - override

    public void StartOwnThread(View view) {

    }


    //Additionally in one that extends Application
    //void onLowMemory();
    //void onTerminate();


    //if onClickListener is implemented
    /*
    @Override
    public void onClick(View v)
    {
        OurNetworkThread demo = new OurNetworkThread();
        demo.setOurNetworkInterface(this);
        demo.start();
    }
    */


//implementing our interfaces
    @Override
    public void operationReady(final String data) {

        MainActivity.this.runOnUiThread( new Runnable(){
            @Override
            public void run(){
                Toast.makeText( MainActivity.this, "Operation is ready: " + data, Toast.LENGTH_LONG).show(); //5);
            } //end.run()
        }); //end

    } //End.operationReady() -implementation
    @Override
    public void caughtError(int httpErrorCode) {
        Toast.makeText( MainActivity.this, "Regretfully, an error has occurred:" + httpErrorCode, Toast.LENGTH_LONG).show();
    }
} //End.MainActivity{}
