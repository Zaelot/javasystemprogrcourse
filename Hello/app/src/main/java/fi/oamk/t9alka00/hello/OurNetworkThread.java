package fi.oamk.t9alka00.hello;

/**
 * Created by t9alka00 on 27.9.2016.
 */

public class OurNetworkThread extends Thread {

    private OurNetworkInterface intf = null;

    public void setOurNetworkInterface( OurNetworkInterface interf ) { this.intf = interf; }

    public void run() {
        try {
            sleep(5000); //5s
            intf.operationReady("Slept for 5 secs.");
        }
        catch (Exception e) {
            //e.getMessage();
            intf.caughtError(400);
        }
    } //End.run()
} //End.OurNetworkThread{}
