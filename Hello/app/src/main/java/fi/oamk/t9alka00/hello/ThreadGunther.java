package fi.oamk.t9alka00.hello;
import android.widget.Toast;

import java.io.*;

/**
 * Created by t9alka00 on 20.9.2016.
 */

public class ThreadGunther extends Thread {
    boolean running = true;
    //no need for our own communication interface in this case

    public void run()
    {
        while (running) {
            //adviced to run this through main thread, but I don't care to create an interface here
            System.out.println("Gunther tule takaisin");

            //textView.setText("not supported"); //background threads must not interact with GUI

            //apparently this needs to be passed along to a interface... go figure. Example in OurNetworkThread
            /* need to pass instance of MainInstance to it
            MainActivity.this.runOnUiThread( new Runnable(){
               @Override
               public void run(){
                   Toast.makeText( MainActivity.this, "Toast pop-up message", Toast.LENGTH_LONG).show();
                   // change UI elements here - include the textview here

               }
            });
            */


            try {
                sleep(5000);
            }
            catch(Exception e)
            {
                e.printStackTrace();
                running = false;
            }
        }//end.while
    } //End.run()

} //End.ThreadGunther
