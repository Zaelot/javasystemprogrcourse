import java.util.Date;
import java.io.*;
//import java.util.scanner;

public class Thread1 extends Thread
{

  boolean running = true;
  OurCommunicationInterface follower = null; //160920 | Interface examples

  public void setFollower(OurCommunicationInterface ci)
  {
    follower = ci;
  } //End.setFollower()

  public void run()
  {

    while (running) {
      String date = new Date().toString();
      //160920 | best to do this in main thread
      if (follower == null)
        System.out.println("Thread1 --> " + date);
        //System.out.println("Gunther tule takaisin");
      else
        follower.ourStringReporting("Thread1 --> " + date);

      //textView.setText("not supported"); //background threads must not interact with GUI

      //Scanner scanner = new Scaner(System.in);
      //scanner.nextInt();
      try {
        sleep(5000);
      }
      catch(Exception e)
      {
        e.printStackTrace();
        running = false;
      }
    }//end.while

    //to interact with the UI, either call through MainActivity or make a Handle in UI Thread
    //in UI thread
    //Handler mHandler;
    //void onPreExecute() { mHandler = new Handler(); }

  } //End.run()

  //for the UI thread handling example from above
  // void doInBackground() {
  //   mHandler.post(new Runnable() {
  //   @Override
  //   public void run() {
  //       if (pressTime == 0)
  //           displayTime.setText("You missed your ring");
  //         }
  //   });
  // }

  //Another UI example:
  // MainActivity.this.runOnUiThread(new Runnable(){
  //   @Override
  //   public void run(){
  //       // change UI elements here
  //   }
  // });

  //quite similarly, using network should not be directly accessed from non-async thread

} //End.Thread1{}
